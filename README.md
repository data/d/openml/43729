# OpenML dataset: Lending-Club-Loan-Data

https://www.openml.org/d/43729

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I wanted a highly imbalanced dataset to share with others. It has the perfect one for us.
Imbalanced data typically refers to a classification problem where the number of observations per class is not equally distributed; often you'll have a large amount of data/observations for one class (referred to as the majority class), and much fewer observations for one or more other classes (referred to as the minority classes). 
For example, In this dataset, There are way more samples of fully paid borrowers versus not fully paid borrowers. 
Full LendingClub data available from their site.
Content
For companies like Lending Club correctly predicting whether or not a loan will be default is very important. This dataset contains historical data from 2007 to 2015, you can to build a deep learning model to predict the chance of default for future loans. As you will see this dataset is highly imbalanced and includes a lot of features that make this problem more challenging.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43729) of an [OpenML dataset](https://www.openml.org/d/43729). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43729/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43729/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43729/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

